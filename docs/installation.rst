Installation
############

EDGE Installation
=================

1. Please ensure that your system has the :doc:`essential software building packages <system_requirement>`. installed properly before proceeding following installation.

2. Download main codes (72M), databases (7.9G , 13G, 7.7G, 40G, 101M for databases and GOTTCHA_db, NCBI_genomes, bwa_index and SNPdb) and third party tools (~2G) from our SFTP server. The password per request. ::
    
    sftp -o "Port 33001" edge@img-gp.lanl.gov:/data/edge/edge_main_v1.0.tgz  ./  
    sftp -o "Port 33001" edge@img-gp.lanl.gov:/data/edge/edge_v1.0_thirdParty_softwares.tgz  ./  
    sftp -o "Port 33001" edge@img-gp.lanl.gov:/data/edge/edge_pipeline_v1.0.databases.tgz  ./  
    sftp -o "Port 33001" edge@img-gp.lanl.gov:/data/edge/GOTTCHA_db.tgz   ./
    sftp -o "Port 33001" edge@img-gp.lanl.gov:/data/edge/NCBI_genomes.tar.gz   ./    
    sftp -o "Port 33001" edge@img-gp.lanl.gov:/data/edge/bwa_index.tgz   ./  
    sftp -o "Port 33001" edge@img-gp.lanl.gov:/data/edge/SNPdb.tgz   ./   
    
.. warning:: Be patient; the database files are huge.

3. Unpack main archive::

    tar -xvzf edge_main_v1.0.tgz

.. note:: The main directory, edge_v1.0, will be created. 

4. Move the database and third party archives into main directory (edge_v1.0)::

    mv edge_v1.0_thirdParty_softwares.tgz edge_v1.0/
    mv edge_pipeline_v1.0.databases.tgz edge_v1.0/
    mv GOTTCHA_db.tgz edge_v1.0/
    mv bwa_index.tgz edge_v1.0/
    mv SNPdb.tgz edge_v1.0/
    mv NCBI_genomes.tar.gz edge_v1.0/
        
5. Change directory to main directory and unpack databases and third party tools archive::
    
    cd edge_v1.0
    tar -xvzf edge_v1.0_thirdParty_softwares.tgz
    tar -xvzf edge_pipeline.v1.0.databases.tgz 
    tar -xvzf GOTTCHA_db.tgz
    tar -xzvf bwa_index.tgz
    tar -xvzf SNPdb.tgz
    tar -xvzf NCBI_genomes.tar.gz
        
.. note:: To this point, you should see a database directory and a thirdParty directory in the main directory

6. Installing pipeline::

    ./INSTALL.sh

  It will install the following depended :doc:`tools <third_party>`.  
    
  * Assembly
  
    * idba
    * spades

  * Annotation
  
    * prokka
    * RATT
    * tRNAscan
    * barrnap
    * BLAST+
    * blastall
    * phageFinder
    * glimmer
    * aragorn
    * prodigal
    * tbl2asn

  * Alignment
    
    * hmmer
    * infernal
    * bowtie2
    * bwa
    * mummer

  * Taxonomy
  
    * kraken
    * metaphlan
    * Metaphyler
    * kronatools
    * gottcha
    * metascope_plus

  * Phylogeny
  
    * FastTree
    * RAxML

  * Utility
  
    * bedtools
    * R
    * GNU_parallel
    * tabix
    * JBrowse
    * primer3
    * samtools
    * sratoolkit

  * Perl_Modules
  
    * perl_parallel_forkmanager
    * perl_excel_writer
    * perl_archive_zip
    * perl_string_approx 
    * perl_pdf_api2
    * perl_html_template
    * perl_html_parser
    * perl_JSON
    * perl_bio_phylo
    * perl_xml_twig
    * perl_cgi_session

7. Restart the Terminal Session to allow $EDGE_HOME to be exported.  

.. note:: After running INSTALL.sh successfully, the binaries and related scripts will be stored in the ./bin and ./scripts directory. It also writes EDGE_HOME environment vairable into .bashrc or .bash_profile. 
 
.. _apache_configuration:
  
Apache Web Server Configuration 
-------------------------------

1. Install apache2 ::

    For Ubuntu

    > sudo apt-get install apache2

    For CentOS

    > sudo yum -y install httpd

2. Enable apache cgid module ::

    For Ubuntu

    > sudo a2enmod cgid

3. Modify sample apache configuration file: **$EDGE_HOME/edge_ui/apache_conf/edge_apache.conf** alias directories to match install

4. Copy edge_apache.conf to the apache or Insert content into httpd.conf ::

    For Ubuntu

    > cp $EDGE_HOME/edge_ui/apache_conf/edge_apache.conf /etc/apache2/conf-available/
    > ln -s /etc/apache2/conf-available/edge_apache.conf /etc/apache2/conf-enabled/

    For CentOS

    > cp $EDGE_HOME/edge_ui/apache_conf/edge_apache.conf /etc/httpd/conf/conf.d/


4. Modify permissions:  mod permissions on installed directory to match apache user (ex: change from www-data to edge)

5. Restart the apache2 to activate the new configuration ::

    For Ubuntu

    > service apache2 restart

    For CentOS

    > httpd -k stop && httpd -k start


User Management system installation
-----------------------------------

1. create database: userManagement::

    > mysql -p -u root
    mysql> create database userManagement;
    mysql> user userManagement;

2. load userManagement_schema.sql::

    mysql> source userManagement_schema.sql;
    
3. load userManagement_constrains.sql::

    mysql> source userManagement_schema.sql;
	 
4. create an user account :: 

       username: userManagement
       password: MtMmG3Ds2dzyYeQ5
       (change password with new phrase)
    and grant all privileges on database "userManagement" to user userManagement
   
    mysql> CREATE USER 'userManagement'@'localhost' IDENTIFIED BY 'MtMmG3Ds2dzyYeQ5';

    mysql> GRANT ALL PRIVILEGES ON userManagement.* to 'userManagement'@'localhost';


5. Configure tomcat:: 
   
    * copy mysql-connector-java-5.1.34-bin.jar to /usr/share/tomcat/lib/
    
        > cp mysql-connector-java-5.1.34-bin.jar /usr/share/tomcat/lib/
    
    * configure tomcat basic auth to secure /user/admin/register web service
      add lines below to /var/lib/tomcat7/conf/tomcat-users.xml
     
        <role rolename="admin"/>
        <user username="userManagementadmin" password="userManagementWSadmin" roles="admin"/>

    * inactive timeout in /var/lib/tomcat7/conf/web.xml (default is 30mins)
    
        <session-config>
            <session-timeout>30</session-timeout>
        </session-config>

    * add the line below to tomcat /usr/share/tomcat7/bin/catalina.sh to increase PermSize:
        
        JAVA_OPTS="$JAVA_OPTS -Xms256m -Xmx1024m -XX:PermSize=256m -XX:MaxPermSize=512m"
        
    * restart tomcat server
    
        > sudo service tomcat7 restart

    * Deploy userManagementWS to tomcat server

        > cp userManagementWS.war /var/lib/tomcat7/webapps/
        > cp userManagementWS.xml /var/lib/tomcat7/conf/Catalina/localhost/

    * Deploy userManagement to tomcat server
    
        > cp userManagement.war /var/lib/tomcat7/webapps
        
    * change settings in /var/lib/tomcat7/webapps/userManagement/WEB-INF/classes/sys.properties
    
    	ex: email_host=mail.yourdomain.com

    * change the url in /var/lib/tomcat7/webapps/userManagement/js/form.js to 
      'var url = "http://yourDomainName:8080/userManagementWS/user/uniqueEmail"';


.. note:: tomcat files in /var/lib/tomcat7 & /usr/share/tomcat7;

          The tomcat server will automatically decompress the userManagementWS.war 
          and userManagement.war ;
        

6. setup admin user:: 

    * run script createAdminAccount.pl to add admin account with encrypted password to database  
    
        > perl createAdminAccount.pl -e admin@my.com -p admin

7. configure the EDGE to use the user management system

	* edit $EDGE_HOME/edge_ui/cgi-bin/edge_config.tmpl where user_management=1

.. note :: If user management system is not in the same domain with edge. ex: http://www.someother.com/userManagement
           The parameter: edge_user_management_url=http://www.someother.com/userManagement
 
8. enable facebook login function

	* modify $EDGE_HOME/edge_ui/cgi-bin/edge_user_managment.cgi at line 94/95 of the admin_email and password according to #6 above.
	
	* modify $EDGE_HOME/edge_ui/javascript/facebookJDK.js at line 6. of the appId= to facebook app id you created.
	
.. note :: A FACEBOOK app need to be created and configure the domain and webiste which EDGE setup.  
           see `https://developers.facebook.com/ <https://developers.facebook.com/.>`_  and 
           `StackOverflow Q&A <http://stackoverflow.com/questions/16345777/given-url-is-not-allowed-by-the-application-configuration>`_
           
9. optional: configure sendmail to use SMTP to email out of local domain::

    * edit /etc/mail/sendmail.cf  and looking for this line:

        # "Smart" relay host (may be null)
        DS

    * and append the correct server right next to DS (no spaces);

        # "Smart" relay host (may be null)
        DSmail.yourdomain.com

    * Then, restart the sendmail service 

        > sudo service sendmail restart
        