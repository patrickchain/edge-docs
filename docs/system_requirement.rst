.. _sys_requirement:

System requirements
###################
NOTE: The web-based online version of EDGE, found on https://bioedge.lanl.gov/edge_ui/ is run on our own internal servers and is our recommended mode of usage for EDGE. It does not require any particular hardware or software other than a web browser. This segment and the installation segment only apply if you want to run EDGE through Python or Apache 2, or through the CLI. 

The current version of the EDGE pipeline has been extensively tested on a Linux Server with Ubuntu 14.04 and Centos 6.5 and 7.0 operating system and will work on 64bit Linux environments. Perl v5.8 or above is required. Python 2.7 is required. Due to the involvement of several memory/time consuming steps, it requires at least 16Gb memory and at least 8 computing CPUs. A higher computer spec is recommended: 128Gb memory and 16 computing CPUs.

Please ensure that your system has the essential software building packages installed properly before running the installing script.

The following are required installed by system administrator.

Ubuntu 14.04 
============

.. image:: https://design.ubuntu.com/wp-content/uploads/ubuntu-logo14.png
    :width: 200px

1. Install build essential libraries and dependancies::
    
    sudo apt-get install build-essential
    sudo apt-get install libreadline-gplv2-dev
    sudo apt-get install libx11-dev
    sudo apt-get install libxt-dev
    sudo apt-get install libncurses5-dev 
    sudo apt-get install gfortran
    sudo apt-get install inkscape
    sudo apt-get install libwww-perl libxml-libxml-perl
    sudo apt-get install zlib1g-dev zip unzip
    sudo apt-get install libpng-dev
    sudo apt-get install cpanminus
    sudo apt-get install default-jre
    sudo apt-get install firefox
    sudo apt-get install wget curl

2. Install python packages for Metaphlan (Taxonomy assignment software)::
   
    sudo apt-get install python-numpy python-matplotlib python-scipy libpython2.7-stdlib 
    sudo apt-get install ipython ipython-notebook python-pandas python-sympy python-nose
  
3. Install BioPerl::
   
    sudo apt-get install bioperl  
        or
    sudo cpan -i -f CJFIELDS/BioPerl-1.6.923.tar.gz
    
4. Install packages for user management system::

    sudo apt-get install sendmail mysql-client mysql-server phpMyAdmin tomcat7

CentOS 6
========

.. image:: https://scottlinux.com/wp-content/uploads/2011/07/centos6.png
    :width: 200px
    
1. Install dependancies using yum::

    # add epel reporsitory 
    sudo yum -y install epel-release
    su -c 'yum localinstall -y --nogpgcheck http://download1.rpmfusion.org/free/el/updates/6/i386/rpmfusion-free-release-6-1.noarch.rpm http://download1.rpmfusion.org/nonfree/el/updates/6/i386/rpmfusion-nonfree-release-6-1.noarch.rpm'
    sudo yum -y update
    
    sudo yum -y install perl-CPAN expat-devel gcc gcc-c++ kernel-devel inkscape perl-JSON 
    sudo yum -y install libXt-devel.x86_64 gcc-gfortran.x86_64 readline-devel.x86_64
    sudo yum -y install git zip zlib-devel perl-ExtUtils-Embed perl-CGI firefox curl wget 
    sudo yum -y install perl-App-cpanminus libX11-devel.x86_64 unzip java-1.7.0-openjdk
    sudo yum -y install perl-libwww-perl perl-XML-LibXML fuse-exfat exfat–utils libpng libpng-devel

2. Install Bioperl using CPAN::

    sudo cpan -i -f CJFIELDS/BioPerl-1.6.923.tar.gz

3. Install dependent packages for Python.
   EDGE requires several packages (NumPy, Matplotlib, SciPy, IPython, Pandas, SymPy and Nose) to work properly. These packages are available at PyPI (https://pypi.python.org/pypi) for downloading and installing respectively. Or you 
   can install a Python distribution with dependent packages instead. We suggest users to use Anaconda Python distribution. You can download the installers and find more information at their website (`https://store.continuum.io/cshop/anaconda/ <https://store.continuum.io/cshop/anaconda/>`_). The installation is interactive. Type in /opt/apps/anaconda when the script asks for the location to install python.::
             
    bash Anaconda-2.x.x-Linux-x86.sh
    ln -s /opt/apps/anaconda/bin/python /path/to/edge_v1.x/bin/
    
  Create symlink anaconda python to edge/bin. So system will use your python over the system’s. 

5. Install packages for user management system::

    (Not test yet.)
    sudo yum -y install sendmail mysql mysql-server phpmyadmin tomcat

CentOS 7
========

.. image:: https://scottlinux.com/wp-content/uploads/2010/07/centos.png
    :width: 200px

1. Install libraries and dependencies by yum::

    sudo yum -y install epel-release
    sudo yum -y install libX11-devel readline-devel libXt-devel ncurses-devel inkscape 
    sudo yum -y install freetype freetype-devel zlib zlib-devel perl-App-cpanminus git 
    sudo yum -y install blas-devel atlas-devel lapack-devel libpng12 libpng12-devel
    sudo yum -y install perl-XML-Simple perl-JSON expat expat-devel perl-Test-Most
    sudo yum -y install perl-XML-LibXML python-pip numpy numpy-f2py scipy java-1.7.0-openjdk 
 
2. Upgrade python packages by pip::
   
    pip install --upgrade six
    pip install --upgrade scipy
    pip install --upgrade matplotlib

3. Detect outdated CPAN modules::

    cpanm App::cpanoutdated
    cpan-outdated -p | cpanm

4. Install perl modules by cpanm::
    
    cpanm Algorithm::Munkres Archive::Tar Array::Compare Clone CGI Convert::Binary::C GD 
    cpanm GraphViz HTML::Template HTML::TableExtract List::MoreUtils PostScript::TextBlock 
    cpanm SOAP::Lite SVG SVG::Graph Set::Scalar Sort::Naturally Spreadsheet::ParseExcel 
    cpanm XML::Parser::PerlSAX XML::SAX XML::SAX::Writer XML::Simple XML::Twig XML::Writer 
    cpanm Graph Time::Piece BioPerl XML::LibXML

5. Install packages for user management system::

    (Not test yet.)
    sudo yum -y install sendmail mysql mysql-server phpMyAdmin tomcat