Troubleshooting
###############

* In the GUI, if you are trying to enter information into a specific field and it is grayed out or won’t let you, try refreshing the page by clicking the icon in the right top of the browser window.
* Process.log and error.log files may help on the troubleshooting.

Data Migration
--------------
* The preferred method of transferring data to the EDGE appliance is via SFTP.  Using an SFTP client such as FileZilla, connect to port 22 using your system's username and password.
* In the case of very large transfers, you may wish to use a USB hard drive or thumb drive.  
* If the data is being transferred from another LINUX machine, the server will recognize partitions that use the FAT, ext2, ext3, or ext4 filesystems.
* If the data is being transferred from a Windows machine, the partition may use the NTFS filesystem.  If this is the case, the drive will not be recognized until you follow these instructions:
	* Open the command line interface by clicking the Applications menu in the top left corner (or use SSH to connect to the system).
	* Enter the command: ''sudo yum install ntfs-3g ntfs-3g-devel -y''
	* Enter your password if required.
* After a reboot, you should be able to connect your Windows hard drive to the system, and it will mount like a normal disk.